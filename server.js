var express =           require('express')
    , http =            require('http')
    , passport =        require('passport')
    , path =            require('path')
    , rootPath =        require('app-root-path')
    , morgan =          require('morgan')
    , compression =     require('compression')
    , bodyParser =      require('body-parser')
    , methodOverride =  require('method-override')
    , cookieParser =    require('cookie-parser')
    , cookieSession =   require('cookie-session')
    , session =         require('express-session')
    , csrf =            require('csurf')

    //EVENT HANDLER
    , mqtt =            require('mqtt')
    , eventEmitter =    require('events').EventEmitter

    // STORMPATH
    , client =          null
    , homedir =         (process.platform === 'win32') ? process.env.HOMEPATH : process.env.HOME
    , keyfile =         homedir + '/Documents/Hasil Karya/Project/Seestern IT/Stormpath/UDSP API Keys/apiKey-3BRVK7RYXSR22RS5O6XTT4JGF.properties'

    // MQTT
    , serverEventEmitter = new eventEmitter()
    , mqttReceivingEmitter = new eventEmitter()
    , mqttHandlerEmitter = new eventEmitter();  


//MQTT SERVER LISTENER CONFIG
var PORT = 1883
  , HOST = 'localhost'
  , CLIENTID = 'server-listener'
  , SECURE_KEY = rootPath + '/config/tls/tls-key.pem'
  , SECURE_CERT = rootPath + '/config/tls/tls-cert.pem'
  , TRUSTED_CA_LIST = [rootPath + '/config/tls/crt.ca.cg.pem']
  , isConnect = false;

if(process.env.MQTT_TLS == "true"){
    var mqttOptions = {
        port: PORT,
        host: HOST,
        clientId: CLIENTID,
        username: 'guest',
        password: 'guest',
        keepalive: 60,
        reconnectPeriod: 1000,
        protocolId: 'MQIsdp',
        protocolVersion: 3,
        clean: true,
        encoding: 'utf8',
        keyPath: SECURE_KEY,
        certPath: SECURE_CERT,
        rejectUnauthorized : true, 
        ca: TRUSTED_CA_LIST
    }; 
} else {
    var mqttOptions = {
        port: PORT,
        host: HOST,
        clientId: CLIENTID,
        username: 'guest',
        password: 'guest',
        keepalive: 60,
        reconnectPeriod: 1000,
        protocolId: 'MQIsdp',
        protocolVersion: 3,
        clean: true,
        encoding: 'utf8',
        rejectUnauthorized : false, 
    };   
}
//ENDOF: MQTT SERVER LISTENER CONFIG


var app = module.exports = express();

app.locals.appname = 'UDSPlatform_SafeIT_Local-Lite'

app.use(compression());
app.set('views', __dirname + '/');
app.set('view engine', 'jade');
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(methodOverride());
app.use(express.static(path.join(__dirname, 'client')));
app.use(cookieParser());
app.use(session({
    secret: process.env.EXPRESS_SECRET || "Superdupersecret"
}));

var env = process.env.NODE_ENV || 'development';
if ('development' === env || 'production' === env) {
    app.use(csrf());
    app.use(function(req, res, next) {
        res.cookie('XSRF-TOKEN', req.csrfToken());
        next();
    });
}


app.set('port', process.env.PORT || 8000);

var httpServer = http.createServer(app);
//var io = require('socket.io')(httpServer);
httpServer.listen(app.get('port'), function () {;
    console.log('SafeIT Push Server is up and running on port %j', app.get('port'));
    if(process.env.MQTT_TLS == "true")
        console.log('Powered by ExpressJS + Mosquitto with TLS.');
    else
        console.log('Powered by ExpressJS + Mosquitto.');
    
    serverEventEmitter.emit('serverUp');

    require('./server/controllers/controller.mqttHandler.js').mqttHandler(app, mqttReceivingEmitter);
});


serverEventEmitter.on('serverUp', function(){
    mqttServerListener = mqtt.connect(mqttOptions);
    require('./server/controllers/controller.device.actuating.js').deviceActuating(app, mqttServerListener);

    console.log('MQTT Server listener started...');

    mqttServerListener.on('connect', function() {
        isConnect = true;
        console.log("MQTT Server Listener is up..");

        mqttReceivingEmitter.emit('connect', isConnect);
        mqttServerListener.subscribe('devices', { qos: 0 });
        mqttServerListener.subscribe('devices/sensing/+', { qos: 1 });
        mqttServerListener.subscribe('devices/actuating/+', { qos: 1 });
    });
    mqttServerListener.on('close', function(error) {
        if(isConnect == true){
            isConnect = false;
            mqttReceivingEmitter.emit('connect', isConnect);
            console.log("MQTT connection closed");
        }
    });

    mqttServerListener.on('offline', function(error) {
        if(isConnect == true){
            isConnect = false;
            mqttReceivingEmitter.emit('connect', isConnect);
            console.log("MQTT connection is offline");
        }
    });

    mqttServerListener.on('error', function(error) {
      console.log("Error connecting to mqtt");
    });


    // fired when a message is received
    mqttServerListener.on('published', function(packet, client) {
      console.log('Published : ', packet.payload);
    });

    // fired when a client subscribes to a topic
    mqttServerListener.on('subscribed', function(topic, client) {
      console.log('subscribed : ', topic);
    });


    mqttServerListener.on('message', function(topic, message, packet) {
        temp = topic.toString().split("/");
        console.log("temp length: ", temp.length);
        console.log("temp length: ", temp);
        if(temp.length > 1){
            tTopic = temp[0] + "/" + temp[1];
            if(temp[2] == "+")
                tReceiver = "*";
            else 
                tReceiver = temp[2];

            objMessage = JSON.parse(message.toString());
            var incommingMessage = {
                topic: tTopic,
                receiver: tReceiver,
                message: objMessage
            }
        } else {
            tTopic = topic;
        }

        mqttReceivingEmitter.emit('incomming', incommingMessage);
        if(tTopic === "devices/sensing/" || tTopic === "devices/sensing")
            mqttReceivingEmitter.emit('sensing', incommingMessage);
        if(tTopic === "devices/actuating/" || tTopic === "devices/actuating")
            mqttReceivingEmitter.emit('actuating', incommingMessage);
        console.log("In the topic: ["+tTopic+"], receive the message: "+message.toString());
    });
});



