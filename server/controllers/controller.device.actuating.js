var serviceDeviceActuating =          require('../services/service.device.actuating.js')
  

module.exports.deviceActuating = function(app, mqttServerListener){
  app.get('/devices/actuating/:id/:pin/:value', function(req, res, next){
	  console.log("Please control: %j device, on pin: %j, and give it value: %j", req.params.id, req.params.pin, req.params.value)
    serviceDeviceActuating.mqttUnsecureSendActuatingCommand(mqttServerListener, req.params.id, req.params.pin, req.params.value, function(err, rRes){
      if(err) res.send(403, err);
      else {
        console.log(rRes);
        res.send(200);
      }
    });
  });
  
};