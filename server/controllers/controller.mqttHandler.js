var serviceMqttHandler = require('../services/service.mqttHandler.js')

module.exports.mqttHandler = function(app, mqttReceivingEmitter){
	var isConnect;

	mqttReceivingEmitter.on('connect', function(isConnect) {
        console.log("connect ahai: %j", isConnect)
    });
    mqttReceivingEmitter.on('incomming', function(error) {
        console.log("incomming ahai")
    });
    mqttReceivingEmitter.on('sensing', function(rPayload) {
        console.log("sensing ahai");
        serviceMqttHandler.log(rPayload.topic, rPayload.message, function(err, rResult){
            if(err !== null) return console.log("Cannot write sensing because the device is unknown");
            console.log(rResult);
          });
    });
    mqttReceivingEmitter.on('actuating', function(error) {
        console.log("actuating ahai")
    });
};