var   serviceMQTT = require('../services/service.mqttHandler.js');
    
var serviceDeviceActuating = module.exports = {
    mqttUnsecureSendActuatingCommand: function(mqttServerListener, pId, pPin, pValue, pCallback){
        //MQTT SERVICE
        rPayload = serviceDeviceActuating.mqttCreateCommandDataStructure(pId, pPin, pValue);
        console.log(rPayload);
        serviceMQTT.publishUnsecure(mqttServerListener, rPayload.topic, rPayload.message, function(err, rResult){
          if(err !== null) return pCallback("Cannot write actuating because the device is unknown");
          
          pCallback(null, rResult)
        });
    },

    mqttCreateCommandDataStructure: function(pId, pPin, pValue){
        var tReceiver = pId;
        var tMessage = '"actuating": {"pin":'+pPin+', "value":'+pValue+'}';
        var tTopic = 'devices/actuating/'+tReceiver;

        return {topic:tTopic, message:tMessage}
        
    }

};

