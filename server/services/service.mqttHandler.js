var hash = require('../../node_modules/hash/crc32')

module.exports = {
    log: function(pTopic, pMessage, rCallback) {
      console.log("Topic: %j\n Message: %j", pTopic, pMessage);
      rCallback(null, "Message has been receive successfuly through MQTT Protocol ");
    },

    publishUnsecure: function(mqttServerListener, pTopic, pMessage, rCallback) {
      var tHashed = hash.utf8_encode(pMessage);
      tHashed = hash.crc32(tHashed);
      var CLIENTID = "unknown"

      var tPayload = '{"date":"'+ Date()+'", "checksum":"'+tHashed+'", "sender":"'+CLIENTID+'", '+pMessage+'}';

      mqttServerListener.publish(pTopic, tPayload);
      console.log("Message has been sent successfuly through MQTT Protocol");
      rCallback(null, "Message has been sent successfuly through MQTT Protocol : " + tPayload);
    }

};